CC	?= cc
CFLAGS	 = -g -Wall  `pkg-config --cflags x11 xft` -I/usr/local/include
LIBS	 = `pkg-config --libs x11 xft`

all: mybar

.SUFFIXES: .c .o
.c.o:
	${CC} ${CFLAGS} -c $< -o $@

mybar: mybar.o i3module.o modules.o johnsonfigliodijson.o modules.h i3module.h
	$(CC) $(CFLAGS) mybar.o i3module.o modules.o johnsonfigliodijson.o -o MIB $(LIBS)

clean:
	rm -f *.o MIB
