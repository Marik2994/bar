#ifndef I3_MODULE_H
#define I3_MODULE_H

#include "modules.h"

int i3_setup(struct state*);
void i3_close(struct state*);
void i3_get_workspaces(struct state *s);

#endif
