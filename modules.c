#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "modules.h"

#ifdef __OpenBSD__
# include <sys/ioctl.h>
# include <sys/types.h>
# include <machine/apmvar.h>
# include <fcntl.h>

void
batterymodule(struct state *s)
{
	int fd;
	struct apm_power_info info;

	s->bat = 0;
	s->in_carica = 0;

	if ((fd = open("/dev/apm", O_RDONLY)) == -1) {
		return;
	}

	if (ioctl(fd, APM_IOC_GETPOWER, &info) == -1) {
		fprintf(stderr, "%s: ioctl APM_IOC_GETPOWER failed!\n", getprogname());
		close(fd);
		return;
	}

	close(fd);
	s->bat = info.battery_life;
	s->in_carica = info.ac_state;
}

#else

void
batterymodule (struct state *s){
  FILE *full, *now, *status;
  int n, m;
  int hfull, know;
  char *sfull, *snow, sstatus;

  s->in_carica = 0;
  s->bat = 0;

  status = fopen (STATUS, "r");
  if(status == NULL){
    printf("%s\n", "il file non è aperto");
    return;
  }

  full = fopen(CHARGE_FULL, "r");
  if (full == NULL){
    printf("%s\n","il file full non si apre ");
    fclose(status);
    return;
  }

  now = fopen (CHARGE_NOW, "r");
  if (now == NULL){
    printf ("il file now non si apre ");
    fclose(status);
    fclose (full);
    return;
  }

  fseek(full, 0, SEEK_END);
  fseek(now, 0, SEEK_END);

  n = ftell(full);
  m = ftell(now);

  sfull = malloc(n+1);
  snow = malloc(m+1);

  rewind (full);
  rewind (now);
  fread(sfull, 1, n, full);
  fread (snow, 1, m, now);

  hfull = strtol(sfull, NULL, 10);
  know = strtol(snow, NULL, 10);

  int p = (know*100/ hfull);

  fread(&sstatus, 1, 1, status);

  fclose(full);
  fclose(now);
  fclose(status);

  s->bat = p;
  //printf("in_carica = %s\n", sstatus);
  s->in_carica = (sstatus == 'C' || sstatus == 'F');
}
#endif

void
datemodule(struct state *s){
  time_t t = time(NULL);
  struct tm *now = localtime(&t);
  strftime((char*)s->data, 256, "%d/%m/%Y %H:%M", now);
}
