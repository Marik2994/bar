#ifndef MODULES_H
#define MODULES_H

#include <unistd.h>
#include <time.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>

#define CHARGE_FULL "/sys/class/power_supply/BAT0/charge_full_design"
#define CHARGE_NOW "/sys/class/power_supply/BAT0/charge_now"
#define STATUS "/sys/class/power_supply/BAT0/status"

struct workspace {
  char *name;
  int visible;
  int focused;
  int urgent;
};

struct state {
  char data[256];
  int in_carica;
  int bat;

  // i3
  int i3fd;
  int i3fd_sub;
  struct workspace *w;

  XftColor *fg;
  XftColor *focused;
  XftColor *urgent;
  GC bg;  
};

void	batterymodule(struct state*);
void	datemodule(struct state*);

#endif
