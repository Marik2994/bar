#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <i3/ipc.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "i3module.h"
#include "johnsonfigliodijson.h"

//#ifdef _NET_WM_I3

#define CAP_GROW(cap) ((cap) < 16 ? 16 : (cap) + ((cap)/2))

int
i3_openconn(char *path)
{
  int sock;
  struct sockaddr_un addr;

  if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
    return -1;

  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, path);

  if (connect(sock, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("connec");
    return -1;
  }

  return sock;
}

json_value *
i3_response(int sock)
{
    // leggi la stringa magica
    printf("prima di read #1\n" );
    char i3_reply[6];
    read(sock, &i3_reply, sizeof(i3_reply));
    printf("dopo di read: #1\n" );

    // leggi la lunghezza del messaggio
    uint32_t msg_len;
    printf("prima di read #2\n" );
    read(sock, &msg_len, sizeof(msg_len));
    printf("dopo di read: #2\n" );

    // leggi il tipo del messaggio
    uint32_t msg_type;
    printf("prima di read #3\n" );
    read(sock, &msg_type, sizeof(msg_type));
    printf("dopo di read: #3\n" );

    // alloca spazio per il messaggio
    char *msg = malloc(msg_len + 1);
    if (msg == NULL)
      return NULL;

    // leggo il messaggio
    printf("prima di read #4\n" );
    read(sock, msg, msg_len);
    printf("dopo di read: #4\n" );
    msg[msg_len] = '\0';

    return json_parse(msg);
}

void
i3_send_msg(int sock, uint32_t type, uint32_t len, void *payload)
{
  write(sock, I3_IPC_MAGIC, strlen(I3_IPC_MAGIC));
  write(sock, &len, 4);
  write(sock, &type, 4);
  write(sock, payload, len);
}

void
i3_get_workspaces(struct state *s)
{
  // leggere la risposta su s->i3fd_sub e poi cancellarla,
  json_value *v = i3_response(s->i3fd_sub);
  json_del_value(v);

  // leggere la lista di workspace
  int index = 0;
  char c = '\0';
  i3_send_msg(s->i3fd, I3_IPC_MESSAGE_TYPE_GET_WORKSPACES, 1, &c);

  v = i3_response(s->i3fd);
  for (int i = 0; i < v->as.array.len; ++i) {
    struct json_object *o = v->as.array.v[i]->as.object;

    while (o != NULL) {
      if (!strcmp(o->key->as.string, "name")) {
	if (s->w[index].name != NULL) {
	  free(s->w[index].name);
	}
	s->w[index].name = strdup(o->value->as.string);
	index++;
      }
      if(!strcmp(o->key->as.string, "urgent")){
	s->w[index].urgent = o->value->as.bool;
      }
      if(!strcmp(o->key->as.string, "visible")){
	s->w[index].visible = o->value->as.bool;
      }
      if(!strcmp(o->key->as.string, "focused")){
	s->w[index].focused = o->value->as.bool;
      }

      o = o->next;
    }
  }

  for (int i = index; i < 10; ++i) {
    if (s->w[i].name != NULL){
      free(s->w[i].name);
      s->w[i].name = NULL;
    }
  } 
    
  json_del_value(v);
}

void
i3_subscribe_workspaces(int sock)
{
  char *payload = "[\"workspace\", \"mode\", \"window\"]";
  i3_send_msg(sock, I3_IPC_MESSAGE_TYPE_SUBSCRIBE, strlen(payload), payload);

  json_value *res = i3_response(sock);
  json_dump_valueln(res);
  json_del_value(res);
}

int
i3_setup(struct state *s)
{
  int fds[2];

  pipe(fds);

  switch(fork()) {
  case -1:
    fprintf(stderr, "francesco colpa tua!\n");
    return 0;

  case 0:
    // figlio:
    close(fds[0]);
    if (dup2(fds[1], 1) == -1)
      perror("dup2");
    // exec i3 --get-socketpath
    execlp("i3", "i3", "--get-socketpath", NULL);
    printf("i3 non trovato!\n");
    break;

  default:
    // padre:
    close(fds[1]);
    // leggere l'output del figlio
    char tmp[5];

    printf("sono il padre\n");

    char *buf = NULL;
    size_t cap = 0;
    size_t len = 0;

    printf("sto per leggere da fds[0]\n");
    ssize_t r;
    while ((r = read(fds[0], &tmp, sizeof(tmp))) > 0) {
      printf("ho letto %ld\n", r);
      if (cap < len + r) {
	cap = CAP_GROW(cap);
	buf = realloc(buf, cap);
	if (buf == NULL)
	  return 0;
      }

      memcpy(&buf[len], tmp, r);
      len += r;
    }
    printf("fuori dal loop\n");

    buf[len-1] = '\0';
    s->i3fd     = i3_openconn(buf);
    s->i3fd_sub = i3_openconn(buf);
    free(buf);
    printf("ho aperto le connessioni\n");

    //i3_get_workspaces(s);
    printf("ho letto i ws\n");
    i3_subscribe_workspaces(s->i3fd_sub);
  }

  return 1;
}

void
i3_close(struct state *s)
{
  close(s->i3fd);
  close(s->i3fd_sub);

  for (int i = 0; i < 10; ++i) {
    if (s->w[i].name != NULL)
      free(s->w[i].name);
  }
}
//#else
